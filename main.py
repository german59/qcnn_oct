import os
import cv2
import numpy as np
import multiprocessing as mp
import eyepy as ep
from matplotlib import pyplot as plt


def get_filenames(path):
    X0 = []
    ext = ('vol')
    for i in sorted(os.listdir(path)):
        if i.endswith(ext):
            X0.append(os.path.join(path, i))
    return X0


def get_masks(filename):
    image = filename
    save_path_I = "dataset/Images/"
    save_path_M = "dataset/Masks/"
    save_path_A = "dataset/Annotations/"
    file = os.path.split(filename)
    save_name = str(os.path.splitext(file[1])[0])

    oct_read = Oct.from_heyex_vol(image)
    # print(type(oct_read.meta["PatientID"]))
    y = oct_read.meta["PatientID"]
    # print(y)
    # print(oct_read.meta["PatientID"])
    data = oct_read.bscans[0].scan
    data = np.expand_dims(data, axis=-1)
    zeros = np.zeros((data.shape[0], data.shape[1], 3)).astype('uint8')
    data1 = np.add(data, zeros)
    # Outer Plexiform Layer: OPL
    OPL = np.round(oct_read.bscans[0].annotation["layers"]['OPL']).astype('uint32')
    INL = np.round(oct_read.bscans[0].annotation["layers"]['INL']).astype('uint32')
    # Ellipsoide Zone: EZ
    PR2 = np.round(oct_read.bscans[0].annotation["layers"]['PR2']).astype('uint32')
    PR1 = np.round(oct_read.bscans[0].annotation["layers"]['PR1']).astype('uint32')

    mask = np.zeros((data.shape[0], data.shape[1], 3)).astype('uint8')
    try:
        for i in range(OPL.shape[0]):
            data1[INL[i], i, 1] = 255
            data1[OPL[i], i, 2] = 255
            data1[PR2[i], i, 0] = 255
            data1[PR1[i], i, :] = [0, 255, 255]
            mask[(INL[i]):(OPL[i]), i, :] = [0, 0, 255] if INL[i] > 0 and OPL[i] > 0 else [0, 0, 0]
            mask[(PR1[i]):(PR2[i]), i, :] = [0, 255, 0] if PR1[i] > 0 and PR2[i] > 0 else [0, 0, 0]

        cv2.imwrite(os.path.join(save_path_I, save_name) + ".png", data)
        cv2.imwrite(os.path.join(save_path_M, save_name) + ".png", mask)
        cv2.imwrite(os.path.join(save_path_A, save_name) + ".png", data1)
        return y
    except:
        print(filename)
        pass


def get_annotations_single(file):
    oct_read = ep.import_heyex_vol(file)
    # print(oct_read.meta["PatientID"])
    data = oct_read.bscans[0].scan
    data = np.expand_dims(data, axis=-1)
    zeros = np.zeros((data.shape[0], data.shape[1], 3)).astype('uint8')
    data1 = np.add(data, zeros)
    # Outer Plexiform Layer: OPL
    OPL = oct_read.bscans[0].annotation["layers"]['OPL'].astype('uint32')
    INL = oct_read.bscans[0].annotation["layers"]['INL'].astype('uint32')

    # Ellipsoide Zone: EZ
    PR2 = oct_read.bscans[0].annotation["layers"]['PR2'].astype('uint32')
    PR1 = oct_read.bscans[0].annotation["layers"]['PR1'].astype('uint32')
    mask = np.zeros((data.shape[0], data.shape[1], 3)).astype('uint8')

    for i in range(OPL.shape[0]):
        data1[INL[i], i, 1] = 255
        data1[OPL[i], i, 2] = 255
        data1[PR2[i], i, 0] = 255
        data1[PR1[i], i, :] = [0, 255, 255]
        mask[(INL[i]):(OPL[i]), i, :] = [0, 0, 255] if INL[i] > 0 and OPL[i] > 0 else [0, 0, 0]
        mask[(PR1[i]):(PR2[i]), i, :] = [0, 255, 0] if PR1[i] > 0 and PR2[i] > 0 else [0, 0, 0]

    plt.imshow(data1)
    plt.show()


def main():
    data_path = "OCT1/"
    files = get_filenames(data_path)
    get_annotations_single(files[2])
    pool = mp.Pool(processes=mp.cpu_count())
    dictin = {}
    # y = pool.map(get_masks, files)  # ***** Function *****
    # pool.close()  # multiprocessing
    # pool.join()  # multiprocessing
    # dictin = dict.fromkeys(y)
    # with open("Patients IDs.txt", "w") as file:
    #     for p in dictin.keys():
    #         file.write("%s\n" % p)
    #
    # print(len(dictin.keys()))


if __name__ == '__main__':
    main()
